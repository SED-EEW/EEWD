# Compilation
Compilation is straightforward:
```
ant
```
The jar should be compiled in about four seconds.
